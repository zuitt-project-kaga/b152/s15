let insert = document.getElementById("msg");

console.log("Hello World from s15!")

let numString1 = "5";
let numString2 = "10";
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;

console.log(num1 + num2);
let sum1 = num1 + num2;
console.log(sum1);

let numString3 = numString1 + sum1;
console.log(numString3);

let sampleStr = "Charles";
console.log(sampleStr + num2);

let difference1 = num1 - num3;
console.log(difference1);

let difference2 = numString2 - num2;
console.log(difference2);

let sampleStr2 = "Joseph";

let difference3 = sampleStr2 - num1;
console.log(difference3);

let difference4 = numString2 - numString1;
console.log(difference4);

function subtract(num1, num2) {
    return num1 - num2;
}

let difference5 = subtract(30, 23);
console.log(difference5);

let product1 = num1 * num2;
console.log(product1);

let product2 = numString1 * numString2;
console.log(product2);

let quotient1 = num2 / num4;
console.log(quotient1);

let quotient2 = numString2 / numString1;
console.log(quotient2);

function multiply(num1, num2) {
    return num1 * num2;
}

let product3 = multiply(32, 5);
console.log(product3);

function divide(num1, num2) {
    return num1 / num2;
}

let quotient3 = divide(100, 10);

console.log(quotient3);

console.log(product3 * 0);
console.log(quotient3 / 0);

console.log(5 % 5);
console.log(25 % 6);
console.log(50 % 2);

let modulo1 = 5 % 6;
console.log(modulo1);

let variable = "initial value";
variable = "new value";
console.log(variable);

let sample1 = "sample value";
variable = sample1;
console.log(variable);
console.log(sample1);

let sample2 = "new sample value";
variable = sample2;
console.log(variable);
console.log(sample2);

// const pi = 3.1426;
// pi = 5000;
// console.log(pi);

let sum2 = 10;
sum2 += 20;
console.log(sum2);

// console.log(5+=5);

let sum4 = 30;
sum4 += "Curry";
console.log(sum4);

let sum5 = "50";
sum5 += "50";
console.log(sum5);

let fullName = "Wardell";
let name1 = "Stephen";

fullName += name1;
console.log(fullName);
fullName += "Curry";
console.log(fullName);

let numSample = 50;
numSample -= 10;
console.log(numSample);

let text = "ChickenDinner";
text -= "Dinner";
console.log(text);

let sampleNum1 = 3;
let sampleNum2 = 4;
sampleNum1 *= sampleNum2;
console.log(sampleNum1);
console.log(sampleNum2);

let mdasResult = 1 + 2 - 3 * 4 / 5;
console.log(mdasResult);

let z = 1;
console.log(z++);
console.log(z);
z++;
console.log(++z);
console.log(z++);
console.log(z);

// console.log(5++); error

let sampleNumString = "5";
console.log(++sampleNumString)

let sampleString = "James";
console.log(sampleString++);

console.log(1 == 1);

let isSame = 55 == 55;
console.log(isSame);

console.log(1 == "1");

console.log(0 == false);
console.log(1 == true);

let sampleConvert = Number(false);
console.log(sampleConvert);

let sampleConvert2 = "2500";
sampleConvert2 = Number(sampleConvert2);
console.log(sampleConvert2);

console.log(1 == true);

console.log(5 == "5");

console.log(true == "true");
console.log(false == "false");

console.log(1 == "1");
console.log(1 === "1");

console.log("james2000" === "James2000");
console.log(55 === "55");

console.log(1 != "1");

console.log("James" != "John");

console.log(5 != 55);
console.log(1500 != "5000");

console.log(true != "true");
console.log(5 !== 5);

let user1 = { username: "peterphoenix_1999", age: 28, level: 15, isGuildAdmin: false }
let user2 = { username: "kinOrodie00", age: 13, level: 50, isGuildAdmin: true }

console.log(user1.username);
console.log(user1.level);

let authorization1 = user1.age >= 18 && user1.level >= 25;
console.log(authorization1);

let authorization2 = user2.age >= 18 && user2.level >= 25;
console.log(authorization2);

let authorization3 = user1.age >= 10 && user1.isGuildAdmin === true;
console.log(authorization3);

// if(user1.age >= 18){
// 	alert("You are allowed to enter!");
// };


if (user1.isGuildAdmin === true) {
    console.log("Welcome back, Guild Admin.");
} else {
    console.log("You are not authorized to enter");
};





let usernameInput1 = "nicoleIsAwesome100";
let passwordInput1 = "iamawesomenicole";

let usernameInput2 = "";
let passwordInput2 = null;

function register(username, password) {
    if (username === "" || password === null) {
        console.log("Please complete the form.");
    } else {
        console.log("Thank you for registering.");
    }
}

register(usernameInput1, passwordInput1);
register(usernameInput2, passwordInput1);

function requirementchecker(level, isGuildAdmin) {
    if (level < 25 && isGuildAdmin === false) {
        console.log("Welcome to the Newbies Guild");
    } else if (level > 25) {
        console.log("You are too strong.");
    } else if (isGuildAdmin === true) {
        console.log("You are a guild admin.");
    }
}

requirementchecker(user1.level, user1.isGuildAdmin);
requirementchecker(user2.level, user2.isGuildAdmin);

let user3 = {
    username: "richieBillions",
    age: 20,
    level: 20,
    isGuildAdmin: true,
}

requirementchecker(user3.level, user3.isGuildAdmin);

function addNum(num1,num2){
	if(typeof num1 === "number" && typeof num2 === "number"){
		console.log("Run only if both arguments passed are number type");
	}else{
		console.log("One or both of the arguments are not numbers.");
	}
}

addNum(5,10);
addNum("6",20);

let str = "sample";
console.log(typeof str);
console.log(typeof 78);

function dayChecker(argument) {
	switch(argument.toLowerCase()){
		case "Sunday":
		console.log("Today is Sunday; Wear white");
		break;
		case "Monday":
		console.log("Today is Monday; Wear Blue");
		break;
		case "Tuesday":
		console.log("Today is Tuesday; Wear Green");
		break;
		case "Wednesday":
		console.log("Today is Wednesday; Wear Purple");
		break;
		case "Thursday":
		console.log("Today is Thursday; Wear Brown");
		break;
		case "friday":
		console.log("Today is Friday; Wear Red");
		break;
		case "Saturday":
		console.log("Today is Saturday; Wear Pink");
		break;
		default:
		console.log("something wrong");
	}
}

dayChecker("friday");


function capitalChecker(argument) {
	switch(argument){
		case  "philippines":
		console.log("Manila");
		break;
		case  "usa":
		console.log("Washington D.C.");
		break;
		case  "japan":
		console.log("Tokyo");
		break;
		case  "germany":
		console.log("Berlin");
		break;
		default:
		console.log("Input is out of range. Choose another country.");
	}
}

capitalChecker("philippines");


let superHero = "Batman";

superHero === "Batman" ? console.log("You are rich") : console.log("Bruce Wayne is richer than you.");

// superHero === "Superman" ? console.log("Hi,Clark!")








insert.innerHTML = "go";


